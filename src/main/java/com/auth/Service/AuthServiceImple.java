package com.auth.Service;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.auth.exception.EmptyInputException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.auth.Doa.AuthUserRepositoryDoa;
import com.auth.Entity.UserAuth;

@Service
public class AuthServiceImple implements AuthService{ //implements the AuthService interface
	@Autowired
    AuthUserRepositoryDoa userRepo;
	@Autowired
	UserAuth user;

	private static int generatedOtp;


	@Override
	public int generateOTP() {  //generates OTP and sends the integer value of OTP
		// TODO Auto-generated method stub
		String numbers = "1234567890";
	      Random random = new Random();
	      char[] otp = new char[6];

	      for(int i = 0; i< 6 ; i++) {
	         otp[i] = numbers.charAt(random.nextInt(numbers.length()));
	      }
	      return Integer.parseInt(String.valueOf(otp));
	}

	@Override
	public boolean verifyOtp(int generatedOtp, int UserGivenOtp) {  //verifies generated OTP with user given OTP
		// TODO Auto-generated method stub
		return generatedOtp==UserGivenOtp;
	}

	@Override
	public String generateAuthToken() {  //generates Auth token
		// TODO Auto-generated method stub
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}

	@Override
	public int checkUserExists(String UserDeviceDetails) {
		// TODO Auto-generated method stub
		return userRepo.existsByUserDeviceDetails(UserDeviceDetails);	//checks if user details exists in the auth database
    }

	@Override
	public int verifyTokenExpiry(String UserDeviceDetails) {
		// TODO Auto-generated method stub
		return userRepo.verifyAuthTokenExpiry(UserDeviceDetails);  //verifies Auth token expiry by comparing current time and expiry time(exists in the database) for given user
	}

	@Override
	public int validateToken(String AuthToken, String deviceId) {
		// TODO Auto-generated method stub
		return userRepo.validateAuthTokenExpiry(AuthToken, deviceId);  //validates auth token for given device-id 7 Auth-token for LOS and Payment Service
	}

	@Override
	public String saveUser(String UserDeviceDetails, long UserId, String AuthToken,String deviceId) {
		// TODO Auto-generated method stub
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
	    calendar.add(Calendar.HOUR, 2);
	    user.setUserDeviceDetails(UserDeviceDetails);
	    user.setUserId(UserId);
	    user.setAuthToken(AuthToken);
	    user.setExpiryTime(new Timestamp(calendar.getTimeInMillis()));
		user.setDeviceId(deviceId);
	    userRepo.save(user);
	    return user.getAuthToken();  // saves user-device-details,user-id,auth-token,expiry-time(current-time+2 hours) in the auth database
	}

	@Override
	public int generateDeviceID() {
		// TODO Auto-generated method stub
		int min=1;
		int max=3;
		return ThreadLocalRandom.current().nextInt(min, max) + min;
	}

	@Override
	public String getValidAuthToken(String UserDeviceDetails) {
		// TODO Auto-generated method stub
		return userRepo.getValidAuthToken(UserDeviceDetails); //returns auth token for a given existing user
	}

	@Override
	public Timestamp getCurrentTime() {
		// TODO Auto-generated method stub
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		return new Timestamp(calendar.getTimeInMillis());  //returns current time
	}

	@Override
	public Timestamp getOtpExpiryTime() {
		// TODO Auto-generated method stub
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.MINUTE, 10);
		return new Timestamp(calendar.getTimeInMillis());  //sets and returns OTP expiry time(user logged in time + 10 minutes)
	}

	@Override
	public int checkOtpExpiry(Timestamp OtpExpiry) {
		// TODO Auto-generated method stub
		return userRepo.CheckOtpExpiry(OtpExpiry);  //checks if user entered OTP within the OTP expiry time
	}

	@Override
	public int getByUserId(String userDeviceDetails) {
		// TODO Auto-generated method stub
		return userRepo.getUserId(userDeviceDetails);  //retrieves user id for a user with given user-device-details
	}

	@Override
	public boolean ChecknumberValidity(String number,String device_model,String device_id) {
		// TODO Auto-generated method stub
		if(number.length()==0||device_id.length()==0||device_model.length()==0)
		{
			throw  new EmptyInputException("400","please Dont leave any input fields empty");
		}

		Pattern p = Pattern.compile("(0|9|8)?[6-9][0-9]{9}");
		  
	    Matcher m = p.matcher(number);
	    return (m.find() && m.group().equals(number));	//ensures that phone number,device model and device id are not empty
		//also ensures that the phone number is of 10 digits and in the given format
	    }

	@Override
	public void storeGeneratedOTP(int generatedOtp) {
		this.generatedOtp=generatedOtp;
	}  //stores generated OTP

	@Override
	public int getStoredGeneratedOTP() {
		return this.generatedOtp;
	}  //returns stored generated OTP to be compared with user entered  OTP in the OtpController

}
