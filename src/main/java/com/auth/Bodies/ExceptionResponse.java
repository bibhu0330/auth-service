package com.auth.Bodies;

import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class ExceptionResponse {
    private String HttpExceptions;
    private int Status;

    public ExceptionResponse() {
        super();
    }

    public ExceptionResponse(String httpExceptions, int status) {
        HttpExceptions = httpExceptions;
        Status = status;
    }

    public String getHttpExceptions() {
        return HttpExceptions;
    }

    public void setHttpExceptions(String httpExceptions) {
        HttpExceptions = httpExceptions;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }
}
